python convertToTex_FinalPlotCorrections.py CorrectionsSpreadsheet_FinalPlotCorrections.csv &> GeneratedCorrections_FinalPlotCorrections.tex

pdflatex --shell-escape CallumKilby_Thesis_Corrections_FinalPlotCorrections_v1.tex
wait
pdflatex --shell-escape CallumKilby_Thesis_Corrections_FinalPlotCorrections_v1.tex

if [ "$(grep Traceback GeneratedCorrections.tex)" ]; then
   echo "CK: There was an error with the python generation of the tex"
fi
