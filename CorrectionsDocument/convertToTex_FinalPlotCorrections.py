# TO DO:
#  * Input section and subsection naming

#!/usr/bin/env python

import sys

# variables to define or keep track of chosen naming conventions
#   If I decide to change naming conventions, change these variables
fs='~' # Field separator used in csv file

corrAppMarker = 'A' # Text marker used to indicate that a correction goes in the corrections appendix

genPage = 'PAll'

chapDict = {'Al':'Whole thesis',
            'Ab':'Abstract',
            'Ap':'Appendix',
            'Bb':'Bibliography',
            'C1':'Introduction',
            'C2':'Theory',
            'C3':'LHC and ATLAS',
            'C4':'Event Sim and Object Reconstruction',
            'C5':'ID Trigger Work',
            'C6':'$t\\bar{t}H(b\\bar{b})$ Analysis',
            'C7':'Conclusions'}

secDict = {'C2.1':'Standard Model',
           'C2.2':'EWSB and Higgs Mechanism',
           'C2.3':'Higgs Coupling To Top Quarks',
           'C2.4':'Higgs Production and $t\bar{t}H$',

           'C3.1':'LHC',
           'C3.2':'ATLAS',

           'C4.1':'Event Sim',
           'C4.2':'Object Reconstruction',

           'C5.1':'Introduction',
           'C5.2':'The Inner Detector Trigger',
           'C5.3':'Track Quality Selection Study',
           'C5.4':'Reduced Track Find Efficiency and RoI $\Delta \phi$',
           'C5.5':'Parametrised Methods Based On Track Seeds',

           'C6.1':'Introduction',
           'C6.2':'Analysis Challenges',
           'C6.3':'Analysis Strategy',
           'C6.4':'Object Definitions and Event Selection',
           'C6.5':'Data And Simulated Event Datasets',
           'C6.6':'Systematic Uncertainties',
           'C6.7':'Statistical Fit',
           'C6.8':'Results'}

subsecDict = {'C3.2.1':'Vertexing and Tracking',
              'C3.2.2':'EM Calorimetry',
              'C3.2.3':'Hadronic Calorimetry',
              'C3.2.4':'Muon Spectrometry',
              'C3.2.5':'TDAQ',

              'C4.1.1':'Event Generation',
              'C4.1.2':'Detector Simulation',

              'C4.2.1':'Track and Vertex Reconstruction',
              'C4.2.2':'Electrons',
              'C4.2.3':'Muons',
              'C4.2.4':'Jets',
              'C4.2.5':'b-jets and Flavour Tagging',
              'C4.2.6':'Taus',
              'C4.2.7':'MET',

              'C5.2.1':'Tau-Stage Tracking Method',
              'C5.2.2':'Tau Lepton Triggers',

              'C6.4.1':'Choice Of Triggers Study',

              'C6.5.1':'Fake and Non-Prompt Lepton Estimate',

              'C6.6.1':'Experimental Uncertainties',
              'C6.6.2':'Modelling Uncertainties',
              'C6.6.3':'PDF Uncertainties Study',

              'C6.7.1':'Initial Fit Model',
              'C6.7.2':'Control Region Binning',
              'C6.7.3':'First Fits to Data Using BDTs',
              'C6.7.4':'Decorrelation of JER Uncertainties',
              'C6.7.5':'Understanding Parton Shower Pulls'}


def generateCorrLabel(page, ref, general, author, isCorrApp = False) :
    if int(general) > 0 : # Ignore page number and per-page reference for general comments, and assume all non-general comments have general == 00
        return '-'.join(['G'+general, author])
    elif isCorrApp :
        return '-'.join([page, ref, 'A', 'G'+general, author])
    else :
        return '-'.join([page, ref, 'G'+general, author]) # assumes general reference is just ## number, and therefore creates label with G##


def printCorr(corrlabel, corr, change) :
    print '  \\item \\textbf{'+corrlabel+'}: \\\\'
    print '    \\textbf{Correction:} '+corr+' \\\\'
    print '    \\textbf{Change:} '+change


def processCsv(f, doCorrApp) :
    global_chapter = ''
    global_sec     = ''
    global_subsec  = ''
    global_page    = ''
    global_corrRef = 1

    firstGlobalPageEnd = True # Hacky way of avoiding final \end{itemize} of general corrections being printed when printing the corrections appendix
    
    for line in f :
        line_split = line.split(fs)

        if (not doCorrApp and line_split[8] == corrAppMarker) or (doCorrApp and not line_split[8] == corrAppMarker) :
            continue # skip any corrections marked for the correction appendix in normal corrections print out, and vice versa

        page     = line_split[0]
        ref      = line_split[1]
        general  = line_split[2]
        chapnum  = line_split[3]
        corrtype = line_split[4]
        author   = line_split[5]
        corr     = line_split[6]
        change   = line_split[7].strip() # in case there's a trailing newline (if this is the last row entry)

        #corrlabel = '-'.join([page, ref, general, chapnum, corrtype, author])
        #corrlabel = '-'.join([page, ref, 'G'+general, author]) # assumes general reference is just ## number, and therefore creates label with G##

        chapter = chapnum
        chapIsNumbered = (chapnum.find('C') != -1 ) # check if this is a chapter of the form C#.#.#
        if chapIsNumbered :
            chapter = chapnum[0:2]
            sec     = chapnum[0:4]
            subsec  = chapnum[0:6]

            chapnum_split = chapnum.split('.')
            sec_num    = chapnum_split[1]
            subsec_num = chapnum_split[2]

        # Handling for general corrections
        if page == genPage :
            # end previous group of corrections
            if chapter != global_chapter and global_chapter != '' :
                print '  \\end{itemize}'
                print ''
            
            # Start new subsection for next chapter
            if chapter != global_chapter :
                print '\\subsection{'+chapDict[chapter]+'}'
                print '  \\begin{itemize}'
                global_chapter = chapter # potential bug if last global changes chapter is same as first per-page changes chapter, because a \section for per-page changes won't be created

            corrlabel = generateCorrLabel(page, ref, general, author)
            printCorr(corrlabel, corr, change)

        # Handling for per-page corrections
        else :
            # end previous group of corrections
            if page != global_page :
                if True and firstGlobalPageEnd : #doCorrApp and firstGlobalPageEnd : # assume that corrections appendix has no general corrections, and therefore do not print initial \end{itemize}
                    firstGlobalPageEnd = False
                else :
                    print '  \\end{itemize}'
                    print ''

            # Start new section for next chapter
            if chapter != global_chapter :
                if doCorrApp : # Make chapters in corrections appendix sub-sections to avoid confusion in corrections document
                    print '\\subsection{'+chapDict[chapter]+'}'
                else :
                    print '\\section{'+chapDict[chapter]+'}'
                global_chapter = chapter

            # # if relevant, start new subsection for next thesis section
            # if (chapIsNumbered and sec_num != '0') :
            #     if sec != global_sec :
            #         if page == global_page : # in case there are corrections on a single page that are split by the start of a section
            #             print '  \\end{itemize}'
            #             print ''
            #             global_page = ''
                    
            #         print '\\subsection{'+secDict[sec]+'}'
            #         global_sec = sec

            #     # if relevant, start new subsubsection for next thesis subsection
            #     if subsec_num != '0' and subsec != global_subsec :
            #         if page == global_page : # in case there are corrections on a single page that are split by the start of a subsection
            #             print '  \\end{itemize}'
            #             print ''
            #             global_page = ''
                    
            #         print '\\subsubsection{'+subsecDict[subsec]+'}'
            #         global_subsec = subsec

            # start new page grouping for next page
            if page != global_page :                
                page_num = page.split('P')[1].lstrip('0')
                hD_page_num = str( 2*int(page_num) - 1 ) # page number for pdf document which includes comment summary pages after every thesis page
            
                #print '  \\hyperlink{thesis.'+hD_page_num+'}{\\textbf{Page '+page_num+'}}'
                print '  \\hyperlink{thesis.'+str(int(page_num)+1)+'}{\\textbf{Page '+page_num+'}}'
                print '  \\begin{itemize}'
                global_page    = page
                global_corrRef = 1

            # Print current correction
            corrlabel = generateCorrLabel(page, "{:02d}".format(global_corrRef), general, author, doCorrApp) # assumes this line wil only be reached for doCorrApp is True when a correction is marked for the corrections appendix
            printCorr(corrlabel, corr, change)

            global_corrRef += 1

    # Close itemize of final correction
    print '  \\end{itemize}'


def convertToTex(inFile) :
    with open(inFile, 'r') as f :
        f.readline() # skip row containing column titles

        processCsv(f, False)
            

if __name__ == "__main__" :
    if len(sys.argv) < 2 :
        print "Please input csv of thesis corrections to convert to Tex"
    else :
        inFile = sys.argv[1]
        convertToTex(inFile)
