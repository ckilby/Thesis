python convertToTex.py CorrectionsSpreadsheet.csv &> GeneratedCorrections.tex

pdflatex --shell-escape CallumKilby_Thesis_Corrections_v2.tex
wait
pdflatex --shell-escape CallumKilby_Thesis_Corrections_v2.tex

if [ "$(grep Traceback GeneratedCorrections.tex)" ]; then
   echo "CK: There was an error with the python generation of the tex"
fi
