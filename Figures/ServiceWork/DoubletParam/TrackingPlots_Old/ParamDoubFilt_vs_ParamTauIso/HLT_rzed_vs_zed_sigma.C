void HLT_rzed_vs_zed_sigma()
{
//=========Macro generated from canvas: canvas-35/histogram
//=========  (Sun Aug 19 16:08:15 2018) by ROOT version6.08/02
   TCanvas *canvas-35 = new TCanvas("canvas-35", "histogram",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   canvas-35->SetHighLightColor(2);
   canvas-35->Range(-358.8477,-0.009402057,299.5885,0.1858601);
   canvas-35->SetFillColor(0);
   canvas-35->SetBorderMode(0);
   canvas-35->SetBorderSize(2);
   canvas-35->SetTickx(1);
   canvas-35->SetTicky(1);
   canvas-35->SetLeftMargin(0.14);
   canvas-35->SetRightMargin(0.05);
   canvas-35->SetTopMargin(0.05);
   canvas-35->SetBottomMargin(0.15);
   canvas-35->SetFrameBorderMode(0);
   canvas-35->SetFrameBorderMode(0);
   
   TH1D *sigma__176 = new TH1D("sigma__176","",6,-200,200);
   sigma__176->SetBinContent(2,0.07840865);
   sigma__176->SetBinContent(3,0.088154);
   sigma__176->SetBinContent(4,0.07988324);
   sigma__176->SetBinContent(5,0.1289624);
   sigma__176->SetBinError(2,0.005447036);
   sigma__176->SetBinError(3,0.002470309);
   sigma__176->SetBinError(4,0.002395825);
   sigma__176->SetBinError(5,0.01066131);
   sigma__176->SetMinimum(0.01988726);
   sigma__176->SetMaximum(0.176097);
   sigma__176->SetEntries(6);
   sigma__176->SetStats(0);
   sigma__176->SetLineWidth(2);
   sigma__176->SetMarkerStyle(20);
   sigma__176->SetMarkerSize(1.2);
   sigma__176->GetXaxis()->SetTitle("Offline z [mm]");
   sigma__176->GetXaxis()->SetRange(0,7);
   sigma__176->GetXaxis()->SetMoreLogLabels();
   sigma__176->GetXaxis()->SetLabelFont(42);
   sigma__176->GetXaxis()->SetLabelSize(0.05);
   sigma__176->GetXaxis()->SetTitleSize(0.05);
   sigma__176->GetXaxis()->SetTitleOffset(1.5);
   sigma__176->GetXaxis()->SetTitleFont(42);
   sigma__176->GetYaxis()->SetTitle("z_{0} resolution [mm]");
   sigma__176->GetYaxis()->SetMoreLogLabels();
   sigma__176->GetYaxis()->SetLabelFont(42);
   sigma__176->GetYaxis()->SetLabelSize(0.05);
   sigma__176->GetYaxis()->SetTitleSize(0.05);
   sigma__176->GetYaxis()->SetTitleOffset(1.5);
   sigma__176->GetYaxis()->SetTitleFont(42);
   sigma__176->GetZaxis()->SetLabelFont(42);
   sigma__176->GetZaxis()->SetLabelSize(0.05);
   sigma__176->GetZaxis()->SetTitleSize(0.05);
   sigma__176->GetZaxis()->SetTitleFont(42);
   sigma__176->Draw("ep");
   
   TH1D *sigma__177 = new TH1D("sigma__177","",6,-200,200);
   sigma__177->SetBinContent(2,0.07687589);
   sigma__177->SetBinContent(3,0.08685543);
   sigma__177->SetBinContent(4,0.07984932);
   sigma__177->SetBinContent(5,0.1289624);
   sigma__177->SetBinError(2,0.005340458);
   sigma__177->SetBinError(3,0.002436062);
   sigma__177->SetBinError(4,0.002399029);
   sigma__177->SetBinError(5,0.01066131);
   sigma__177->SetMinimum(0.01988726);
   sigma__177->SetMaximum(0.176097);
   sigma__177->SetEntries(6);
   sigma__177->SetDirectory(0);
   sigma__177->SetStats(0);
   sigma__177->SetLineStyle(2);
   sigma__177->SetLineWidth(2);
   sigma__177->SetMarkerStyle(0);
   sigma__177->SetMarkerSize(1.2);
   sigma__177->GetXaxis()->SetTitle("Offline z [mm]");
   sigma__177->GetXaxis()->SetRange(0,7);
   sigma__177->GetXaxis()->SetLabelFont(42);
   sigma__177->GetXaxis()->SetLabelSize(0.05);
   sigma__177->GetXaxis()->SetTitleSize(0.05);
   sigma__177->GetXaxis()->SetTitleOffset(1.5);
   sigma__177->GetXaxis()->SetTitleFont(42);
   sigma__177->GetYaxis()->SetTitle("z_{0} resolution [mm]");
   sigma__177->GetYaxis()->SetMoreLogLabels();
   sigma__177->GetYaxis()->SetLabelFont(42);
   sigma__177->GetYaxis()->SetLabelSize(0.05);
   sigma__177->GetYaxis()->SetTitleSize(0.05);
   sigma__177->GetYaxis()->SetTitleOffset(1.5);
   sigma__177->GetYaxis()->SetTitleFont(42);
   sigma__177->GetZaxis()->SetLabelFont(42);
   sigma__177->GetZaxis()->SetLabelSize(0.05);
   sigma__177->GetZaxis()->SetTitleSize(0.05);
   sigma__177->GetZaxis()->SetTitleFont(42);
   sigma__177->Draw("hist same][");
   
   TH1D *sigma__178 = new TH1D("sigma__178","",6,-200,200);
   sigma__178->SetBinContent(2,0.07840865);
   sigma__178->SetBinContent(3,0.088154);
   sigma__178->SetBinContent(4,0.07988324);
   sigma__178->SetBinContent(5,0.1289624);
   sigma__178->SetBinError(2,0.005447036);
   sigma__178->SetBinError(3,0.002470309);
   sigma__178->SetBinError(4,0.002395825);
   sigma__178->SetBinError(5,0.01066131);
   sigma__178->SetMinimum(0.01988726);
   sigma__178->SetMaximum(0.176097);
   sigma__178->SetEntries(6);
   sigma__178->SetStats(0);
   sigma__178->SetLineWidth(2);
   sigma__178->SetMarkerStyle(20);
   sigma__178->SetMarkerSize(1.2);
   sigma__178->GetXaxis()->SetTitle("Offline z [mm]");
   sigma__178->GetXaxis()->SetRange(0,7);
   sigma__178->GetXaxis()->SetMoreLogLabels();
   sigma__178->GetXaxis()->SetLabelFont(42);
   sigma__178->GetXaxis()->SetLabelSize(0.05);
   sigma__178->GetXaxis()->SetTitleSize(0.05);
   sigma__178->GetXaxis()->SetTitleOffset(1.5);
   sigma__178->GetXaxis()->SetTitleFont(42);
   sigma__178->GetYaxis()->SetTitle("z_{0} resolution [mm]");
   sigma__178->GetYaxis()->SetMoreLogLabels();
   sigma__178->GetYaxis()->SetLabelFont(42);
   sigma__178->GetYaxis()->SetLabelSize(0.05);
   sigma__178->GetYaxis()->SetTitleSize(0.05);
   sigma__178->GetYaxis()->SetTitleOffset(1.5);
   sigma__178->GetYaxis()->SetTitleFont(42);
   sigma__178->GetZaxis()->SetLabelFont(42);
   sigma__178->GetZaxis()->SetLabelSize(0.05);
   sigma__178->GetZaxis()->SetTitleSize(0.05);
   sigma__178->GetZaxis()->SetTitleFont(42);
   sigma__178->Draw("ep same");
   
   TLegend *leg = new TLegend(0.18,0.735,0.28,0.85,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.04);
   leg->SetLineColor(0);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(3000);
   TLegendEntry *entry=leg->AddEntry("sigma","Fast track finder","p");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1.2);
   entry->SetTextFont(42);
   entry=leg->AddEntry("sigma","Precision tracking","p");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   leg->Draw();
   
   TH1D *sigma__179 = new TH1D("sigma__179","RMS",6,-200,200);
   sigma__179->SetBinContent(2,0.03117115);
   sigma__179->SetBinContent(3,0.03093258);
   sigma__179->SetBinContent(4,0.02982898);
   sigma__179->SetBinContent(5,0.04525041);
   sigma__179->SetBinError(2,0.002165553);
   sigma__179->SetBinError(3,0.0008673066);
   sigma__179->SetBinError(4,0.0008937083);
   sigma__179->SetBinError(5,0.003740734);
   sigma__179->SetMinimum(0.01988726);
   sigma__179->SetMaximum(0.176097);
   sigma__179->SetEntries(6);
   sigma__179->SetDirectory(0);
   sigma__179->SetStats(0);
   sigma__179->SetLineColor(2);
   sigma__179->SetLineStyle(2);
   sigma__179->SetLineWidth(2);
   sigma__179->SetMarkerStyle(0);
   sigma__179->SetMarkerSize(1.2);
   sigma__179->GetXaxis()->SetTitle("Offline z [mm]");
   sigma__179->GetXaxis()->SetRange(0,7);
   sigma__179->GetXaxis()->SetLabelFont(42);
   sigma__179->GetXaxis()->SetLabelSize(0.05);
   sigma__179->GetXaxis()->SetTitleSize(0.05);
   sigma__179->GetXaxis()->SetTitleOffset(1.5);
   sigma__179->GetXaxis()->SetTitleFont(42);
   sigma__179->GetYaxis()->SetTitle("z_{0} resolution [mm]");
   sigma__179->GetYaxis()->SetMoreLogLabels();
   sigma__179->GetYaxis()->SetLabelFont(42);
   sigma__179->GetYaxis()->SetLabelSize(0.05);
   sigma__179->GetYaxis()->SetTitleSize(0.05);
   sigma__179->GetYaxis()->SetTitleOffset(1.5);
   sigma__179->GetYaxis()->SetTitleFont(42);
   sigma__179->GetZaxis()->SetLabelFont(42);
   sigma__179->GetZaxis()->SetLabelSize(0.05);
   sigma__179->GetZaxis()->SetTitleSize(0.05);
   sigma__179->GetZaxis()->SetTitleFont(42);
   sigma__179->Draw("hist same][");
   
   TH1D *sigma__180 = new TH1D("sigma__180","RMS",6,-200,200);
   sigma__180->SetBinContent(2,0.03194922);
   sigma__180->SetBinContent(3,0.03053607);
   sigma__180->SetBinContent(4,0.02980318);
   sigma__180->SetBinContent(5,0.04525041);
   sigma__180->SetBinError(2,0.002194281);
   sigma__180->SetBinError(3,0.0008568628);
   sigma__180->SetBinError(4,0.0008913365);
   sigma__180->SetBinError(5,0.003740734);
   sigma__180->SetMinimum(0.01988726);
   sigma__180->SetMaximum(0.176097);
   sigma__180->SetEntries(6);
   sigma__180->SetStats(0);
   sigma__180->SetLineColor(2);
   sigma__180->SetLineWidth(2);
   sigma__180->SetMarkerColor(2);
   sigma__180->SetMarkerStyle(24);
   sigma__180->SetMarkerSize(1.2);
   sigma__180->GetXaxis()->SetTitle("Offline z [mm]");
   sigma__180->GetXaxis()->SetRange(0,7);
   sigma__180->GetXaxis()->SetLabelFont(42);
   sigma__180->GetXaxis()->SetLabelSize(0.05);
   sigma__180->GetXaxis()->SetTitleSize(0.05);
   sigma__180->GetXaxis()->SetTitleOffset(1.5);
   sigma__180->GetXaxis()->SetTitleFont(42);
   sigma__180->GetYaxis()->SetTitle("z_{0} resolution [mm]");
   sigma__180->GetYaxis()->SetMoreLogLabels();
   sigma__180->GetYaxis()->SetLabelFont(42);
   sigma__180->GetYaxis()->SetLabelSize(0.05);
   sigma__180->GetYaxis()->SetTitleSize(0.05);
   sigma__180->GetYaxis()->SetTitleOffset(1.5);
   sigma__180->GetYaxis()->SetTitleFont(42);
   sigma__180->GetZaxis()->SetLabelFont(42);
   sigma__180->GetZaxis()->SetLabelSize(0.05);
   sigma__180->GetZaxis()->SetTitleSize(0.05);
   sigma__180->GetZaxis()->SetTitleFont(42);
   sigma__180->Draw("ep same");
   
   leg = new TLegend(0.18,0.735,0.28,0.85,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.04);
   leg->SetLineColor(0);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(3000);
   entry=leg->AddEntry("sigma","Fast track finder","p");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1.2);
   entry->SetTextFont(42);
   entry=leg->AddEntry("sigma","Precision tracking","p");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   leg->Draw();
   TLatex *   tex = new TLatex(0.18,0.87875,"ATLAS");
tex->SetNDC();
   tex->SetTextFont(72);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.3018563,0.87875,"Internal");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetLineWidth(2);
   tex->Draw();
   canvas-35->Modified();
   canvas-35->cd();
   canvas-35->SetSelected(canvas-35);
}
