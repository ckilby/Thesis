void HLT_phi_eff()
{
//=========Macro generated from canvas: canvas-7/histogram
//=========  (Sun Aug 19 15:59:54 2018) by ROOT version6.08/02
   TCanvas *canvas-7 = new TCanvas("canvas-7", "histogram",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   canvas-7->SetHighLightColor(2);
   canvas-7->Range(-4.228123,87.75,3.529901,102.75);
   canvas-7->SetFillColor(0);
   canvas-7->SetBorderMode(0);
   canvas-7->SetBorderSize(2);
   canvas-7->SetTickx(1);
   canvas-7->SetTicky(1);
   canvas-7->SetLeftMargin(0.14);
   canvas-7->SetRightMargin(0.05);
   canvas-7->SetTopMargin(0.05);
   canvas-7->SetBottomMargin(0.15);
   canvas-7->SetFrameBorderMode(0);
   canvas-7->SetFrameBorderMode(0);
   
   TH1F *_eff__36 = new TH1F("_eff__36","",9,-3.142,3.142);
   _eff__36->SetBinContent(1,99.42197);
   _eff__36->SetBinContent(2,98.61111);
   _eff__36->SetBinContent(3,100);
   _eff__36->SetBinContent(4,100);
   _eff__36->SetBinContent(5,100);
   _eff__36->SetBinContent(6,100);
   _eff__36->SetBinContent(7,100);
   _eff__36->SetBinContent(8,100);
   _eff__36->SetBinContent(9,100);
   _eff__36->SetBinError(1,1e-100);
   _eff__36->SetBinError(2,1e-100);
   _eff__36->SetBinError(3,1e-100);
   _eff__36->SetBinError(4,1e-100);
   _eff__36->SetBinError(5,1e-100);
   _eff__36->SetBinError(6,1e-100);
   _eff__36->SetBinError(7,1e-100);
   _eff__36->SetBinError(8,1e-100);
   _eff__36->SetBinError(9,1e-100);
   _eff__36->SetMinimum(90);
   _eff__36->SetMaximum(102);
   _eff__36->SetEntries(9);
   _eff__36->SetStats(0);
   _eff__36->SetLineWidth(2);
   _eff__36->SetMarkerStyle(20);
   _eff__36->SetMarkerSize(1.2);
   _eff__36->GetXaxis()->SetTitle("Offline track #phi");
   _eff__36->GetXaxis()->SetMoreLogLabels();
   _eff__36->GetXaxis()->SetLabelFont(42);
   _eff__36->GetXaxis()->SetLabelSize(0.05);
   _eff__36->GetXaxis()->SetTitleSize(0.05);
   _eff__36->GetXaxis()->SetTitleOffset(1.5);
   _eff__36->GetXaxis()->SetTitleFont(42);
   _eff__36->GetYaxis()->SetTitle("Efficiency [%]");
   _eff__36->GetYaxis()->SetMoreLogLabels();
   _eff__36->GetYaxis()->SetLabelFont(42);
   _eff__36->GetYaxis()->SetLabelSize(0.05);
   _eff__36->GetYaxis()->SetTitleSize(0.05);
   _eff__36->GetYaxis()->SetTitleOffset(1.5);
   _eff__36->GetYaxis()->SetTitleFont(42);
   _eff__36->GetZaxis()->SetLabelFont(42);
   _eff__36->GetZaxis()->SetLabelSize(0.05);
   _eff__36->GetZaxis()->SetTitleSize(0.05);
   _eff__36->GetZaxis()->SetTitleFont(42);
   _eff__36->Draw("ep");
   
   Double_t divide__eff_n_by__eff_d_fx3007[9] = {
   -2.792889,
   -2.094667,
   -1.396444,
   -0.6982222,
   -5.551115e-17,
   0.6982222,
   1.396444,
   2.094667,
   2.792889};
   Double_t divide__eff_n_by__eff_d_fy3007[9] = {
   99.42197,
   98.61111,
   100,
   100,
   100,
   100,
   100,
   100,
   100};
   Double_t divide__eff_n_by__eff_d_felx3007[9] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t divide__eff_n_by__eff_d_fely3007[9] = {
   0.8558666,
   1.261331,
   0.7110337,
   0.7529755,
   0.6855754,
   0.6618771,
   0.6258223,
   0.7629768,
   0.8172518};
   Double_t divide__eff_n_by__eff_d_fehx3007[9] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t divide__eff_n_by__eff_d_fehy3007[9] = {
   0.421465,
   0.7828382,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   TGraphAsymmErrors *grae = new TGraphAsymmErrors(9,divide__eff_n_by__eff_d_fx3007,divide__eff_n_by__eff_d_fy3007,divide__eff_n_by__eff_d_felx3007,divide__eff_n_by__eff_d_fehx3007,divide__eff_n_by__eff_d_fely3007,divide__eff_n_by__eff_d_fehy3007);
   grae->SetName("divide__eff_n_by__eff_d");
   grae->SetTitle("phi");
   grae->SetLineWidth(2);
   grae->SetMarkerStyle(20);
   grae->SetMarkerSize(1.2);
   grae->Draw("e");
   
   TH1F *_eff__37 = new TH1F("_eff__37","",9,-3.142,3.142);
   _eff__37->SetBinContent(1,99.4186);
   _eff__37->SetBinContent(2,99.30556);
   _eff__37->SetBinContent(3,100);
   _eff__37->SetBinContent(4,100);
   _eff__37->SetBinContent(5,100);
   _eff__37->SetBinContent(6,100);
   _eff__37->SetBinContent(7,100);
   _eff__37->SetBinContent(8,100);
   _eff__37->SetBinContent(9,99.28058);
   _eff__37->SetBinError(1,0.5797028);
   _eff__37->SetBinError(2,0.692029);
   _eff__37->SetBinError(9,0.7168319);
   _eff__37->SetMinimum(90);
   _eff__37->SetMaximum(102);
   _eff__37->SetEntries(9);
   _eff__37->SetStats(0);
   _eff__37->SetLineStyle(2);
   _eff__37->SetLineWidth(2);
   _eff__37->SetMarkerStyle(0);
   _eff__37->SetMarkerSize(1.2);
   _eff__37->GetXaxis()->SetTitle("Offline track #phi");
   _eff__37->GetXaxis()->SetLabelFont(42);
   _eff__37->GetXaxis()->SetLabelSize(0.05);
   _eff__37->GetXaxis()->SetTitleSize(0.05);
   _eff__37->GetXaxis()->SetTitleOffset(1.5);
   _eff__37->GetXaxis()->SetTitleFont(42);
   _eff__37->GetYaxis()->SetTitle("Efficiency [%]");
   _eff__37->GetYaxis()->SetMoreLogLabels();
   _eff__37->GetYaxis()->SetLabelFont(42);
   _eff__37->GetYaxis()->SetLabelSize(0.05);
   _eff__37->GetYaxis()->SetTitleSize(0.05);
   _eff__37->GetYaxis()->SetTitleOffset(1.5);
   _eff__37->GetYaxis()->SetTitleFont(42);
   _eff__37->GetZaxis()->SetLabelFont(42);
   _eff__37->GetZaxis()->SetLabelSize(0.05);
   _eff__37->GetZaxis()->SetTitleSize(0.05);
   _eff__37->GetZaxis()->SetTitleFont(42);
   _eff__37->Draw("hist same][");
   
   Double_t divide__eff_n_by__eff_d_fx3008[9] = {
   -2.792889,
   -2.094667,
   -1.396444,
   -0.6982222,
   -5.551115e-17,
   0.6982222,
   1.396444,
   2.094667,
   2.792889};
   Double_t divide__eff_n_by__eff_d_fy3008[9] = {
   99.42197,
   98.61111,
   100,
   100,
   100,
   100,
   100,
   100,
   100};
   Double_t divide__eff_n_by__eff_d_felx3008[9] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t divide__eff_n_by__eff_d_fely3008[9] = {
   0.8558666,
   1.261331,
   0.7110337,
   0.7529755,
   0.6855754,
   0.6618771,
   0.6258223,
   0.7629768,
   0.8172518};
   Double_t divide__eff_n_by__eff_d_fehx3008[9] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t divide__eff_n_by__eff_d_fehy3008[9] = {
   0.421465,
   0.7828382,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   grae = new TGraphAsymmErrors(9,divide__eff_n_by__eff_d_fx3008,divide__eff_n_by__eff_d_fy3008,divide__eff_n_by__eff_d_felx3008,divide__eff_n_by__eff_d_fehx3008,divide__eff_n_by__eff_d_fely3008,divide__eff_n_by__eff_d_fehy3008);
   grae->SetName("divide__eff_n_by__eff_d");
   grae->SetTitle("phi");
   grae->SetLineWidth(2);
   grae->SetMarkerStyle(20);
   grae->SetMarkerSize(1.2);
   grae->Draw("e1");
   
   TH1F *_eff__38 = new TH1F("_eff__38","",9,-3.142,3.142);
   _eff__38->SetBinContent(1,99.42197);
   _eff__38->SetBinContent(2,98.61111);
   _eff__38->SetBinContent(3,100);
   _eff__38->SetBinContent(4,100);
   _eff__38->SetBinContent(5,100);
   _eff__38->SetBinContent(6,100);
   _eff__38->SetBinContent(7,100);
   _eff__38->SetBinContent(8,100);
   _eff__38->SetBinContent(9,100);
   _eff__38->SetBinError(1,1e-100);
   _eff__38->SetBinError(2,1e-100);
   _eff__38->SetBinError(3,1e-100);
   _eff__38->SetBinError(4,1e-100);
   _eff__38->SetBinError(5,1e-100);
   _eff__38->SetBinError(6,1e-100);
   _eff__38->SetBinError(7,1e-100);
   _eff__38->SetBinError(8,1e-100);
   _eff__38->SetBinError(9,1e-100);
   _eff__38->SetMinimum(90);
   _eff__38->SetMaximum(102);
   _eff__38->SetEntries(9);
   _eff__38->SetStats(0);
   _eff__38->SetLineWidth(2);
   _eff__38->SetMarkerStyle(20);
   _eff__38->SetMarkerSize(1.2);
   _eff__38->GetXaxis()->SetTitle("Offline track #phi");
   _eff__38->GetXaxis()->SetMoreLogLabels();
   _eff__38->GetXaxis()->SetLabelFont(42);
   _eff__38->GetXaxis()->SetLabelSize(0.05);
   _eff__38->GetXaxis()->SetTitleSize(0.05);
   _eff__38->GetXaxis()->SetTitleOffset(1.5);
   _eff__38->GetXaxis()->SetTitleFont(42);
   _eff__38->GetYaxis()->SetTitle("Efficiency [%]");
   _eff__38->GetYaxis()->SetMoreLogLabels();
   _eff__38->GetYaxis()->SetLabelFont(42);
   _eff__38->GetYaxis()->SetLabelSize(0.05);
   _eff__38->GetYaxis()->SetTitleSize(0.05);
   _eff__38->GetYaxis()->SetTitleOffset(1.5);
   _eff__38->GetYaxis()->SetTitleFont(42);
   _eff__38->GetZaxis()->SetLabelFont(42);
   _eff__38->GetZaxis()->SetLabelSize(0.05);
   _eff__38->GetZaxis()->SetTitleSize(0.05);
   _eff__38->GetZaxis()->SetTitleFont(42);
   _eff__38->Draw("ep same");
   
   TLegend *leg = new TLegend(0.18,0.18,0.28,0.295,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.04);
   leg->SetLineColor(0);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(3000);
   TLegendEntry *entry=leg->AddEntry("_eff","Fast track finder","p");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1.2);
   entry->SetTextFont(42);
   entry=leg->AddEntry("_eff","Precision tracking","p");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   leg->Draw();
   
   TH1F *_eff__39 = new TH1F("_eff__39","phi",9,-3.142,3.142);
   _eff__39->SetBinContent(1,99.4186);
   _eff__39->SetBinContent(2,99.30556);
   _eff__39->SetBinContent(3,100);
   _eff__39->SetBinContent(4,99.33775);
   _eff__39->SetBinContent(5,100);
   _eff__39->SetBinContent(6,99.4186);
   _eff__39->SetBinContent(7,98.35165);
   _eff__39->SetBinContent(8,99.32886);
   _eff__39->SetBinContent(9,99.28058);
   _eff__39->SetBinError(1,0.5797028);
   _eff__39->SetBinError(2,0.692029);
   _eff__39->SetBinError(4,0.6600551);
   _eff__39->SetBinError(6,0.5797028);
   _eff__39->SetBinError(7,0.9438002);
   _eff__39->SetBinError(8,0.668885);
   _eff__39->SetBinError(9,0.7168319);
   _eff__39->SetMinimum(90);
   _eff__39->SetMaximum(102);
   _eff__39->SetEntries(9);
   _eff__39->SetStats(0);
   _eff__39->SetLineColor(2);
   _eff__39->SetLineStyle(2);
   _eff__39->SetLineWidth(2);
   _eff__39->SetMarkerStyle(0);
   _eff__39->SetMarkerSize(1.2);
   _eff__39->GetXaxis()->SetTitle("Offline track #phi");
   _eff__39->GetXaxis()->SetLabelFont(42);
   _eff__39->GetXaxis()->SetLabelSize(0.05);
   _eff__39->GetXaxis()->SetTitleSize(0.05);
   _eff__39->GetXaxis()->SetTitleOffset(1.5);
   _eff__39->GetXaxis()->SetTitleFont(42);
   _eff__39->GetYaxis()->SetTitle("Efficiency [%]");
   _eff__39->GetYaxis()->SetMoreLogLabels();
   _eff__39->GetYaxis()->SetLabelFont(42);
   _eff__39->GetYaxis()->SetLabelSize(0.05);
   _eff__39->GetYaxis()->SetTitleSize(0.05);
   _eff__39->GetYaxis()->SetTitleOffset(1.5);
   _eff__39->GetYaxis()->SetTitleFont(42);
   _eff__39->GetZaxis()->SetLabelFont(42);
   _eff__39->GetZaxis()->SetLabelSize(0.05);
   _eff__39->GetZaxis()->SetTitleSize(0.05);
   _eff__39->GetZaxis()->SetTitleFont(42);
   _eff__39->Draw("hist same][");
   
   Double_t divide__eff_n_by__eff_d_fx3009[9] = {
   -2.792889,
   -2.094667,
   -1.396444,
   -0.6982222,
   -5.551115e-17,
   0.6982222,
   1.396444,
   2.094667,
   2.792889};
   Double_t divide__eff_n_by__eff_d_fy3009[9] = {
   99.42197,
   98.61111,
   100,
   99.33775,
   100,
   100,
   98.35165,
   100,
   100};
   Double_t divide__eff_n_by__eff_d_felx3009[9] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t divide__eff_n_by__eff_d_fely3009[9] = {
   0.8558666,
   1.261331,
   0.7110337,
   0.9785347,
   0.6855754,
   0.6618771,
   1.157259,
   0.7629768,
   0.8172518};
   Double_t divide__eff_n_by__eff_d_fehx3009[9] = {
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0,
   0};
   Double_t divide__eff_n_by__eff_d_fehy3009[9] = {
   0.421465,
   0.7828382,
   0,
   0.482591,
   0,
   0,
   0.7886183,
   0,
   0};
   grae = new TGraphAsymmErrors(9,divide__eff_n_by__eff_d_fx3009,divide__eff_n_by__eff_d_fy3009,divide__eff_n_by__eff_d_felx3009,divide__eff_n_by__eff_d_fehx3009,divide__eff_n_by__eff_d_fely3009,divide__eff_n_by__eff_d_fehy3009);
   grae->SetName("divide__eff_n_by__eff_d");
   grae->SetTitle("phi");
   grae->SetLineColor(2);
   grae->SetLineWidth(2);
   grae->SetMarkerColor(2);
   grae->SetMarkerStyle(24);
   grae->SetMarkerSize(1.2);
   grae->Draw("e1");
   
   TH1F *_eff__40 = new TH1F("_eff__40","phi",9,-3.142,3.142);
   _eff__40->SetBinContent(1,99.42197);
   _eff__40->SetBinContent(2,98.61111);
   _eff__40->SetBinContent(3,100);
   _eff__40->SetBinContent(4,99.33775);
   _eff__40->SetBinContent(5,100);
   _eff__40->SetBinContent(6,100);
   _eff__40->SetBinContent(7,98.35165);
   _eff__40->SetBinContent(8,100);
   _eff__40->SetBinContent(9,100);
   _eff__40->SetBinError(1,1e-100);
   _eff__40->SetBinError(2,1e-100);
   _eff__40->SetBinError(3,1e-100);
   _eff__40->SetBinError(4,1e-100);
   _eff__40->SetBinError(5,1e-100);
   _eff__40->SetBinError(6,1e-100);
   _eff__40->SetBinError(7,1e-100);
   _eff__40->SetBinError(8,1e-100);
   _eff__40->SetBinError(9,1e-100);
   _eff__40->SetMinimum(90);
   _eff__40->SetMaximum(102);
   _eff__40->SetEntries(9);
   _eff__40->SetStats(0);
   _eff__40->SetLineColor(2);
   _eff__40->SetLineWidth(2);
   _eff__40->SetMarkerColor(2);
   _eff__40->SetMarkerStyle(24);
   _eff__40->SetMarkerSize(1.2);
   _eff__40->GetXaxis()->SetTitle("Offline track #phi");
   _eff__40->GetXaxis()->SetLabelFont(42);
   _eff__40->GetXaxis()->SetLabelSize(0.05);
   _eff__40->GetXaxis()->SetTitleSize(0.05);
   _eff__40->GetXaxis()->SetTitleOffset(1.5);
   _eff__40->GetXaxis()->SetTitleFont(42);
   _eff__40->GetYaxis()->SetTitle("Efficiency [%]");
   _eff__40->GetYaxis()->SetMoreLogLabels();
   _eff__40->GetYaxis()->SetLabelFont(42);
   _eff__40->GetYaxis()->SetLabelSize(0.05);
   _eff__40->GetYaxis()->SetTitleSize(0.05);
   _eff__40->GetYaxis()->SetTitleOffset(1.5);
   _eff__40->GetYaxis()->SetTitleFont(42);
   _eff__40->GetZaxis()->SetLabelFont(42);
   _eff__40->GetZaxis()->SetLabelSize(0.05);
   _eff__40->GetZaxis()->SetTitleSize(0.05);
   _eff__40->GetZaxis()->SetTitleFont(42);
   _eff__40->Draw("ep same");
   
   leg = new TLegend(0.18,0.18,0.28,0.295,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.04);
   leg->SetLineColor(0);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(3000);
   entry=leg->AddEntry("_eff","Fast track finder","p");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1.2);
   entry->SetTextFont(42);
   entry=leg->AddEntry("_eff","Precision tracking","p");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   leg->Draw();
   TLatex *   tex = new TLatex(0.18,0.32375,"ATLAS");
tex->SetNDC();
   tex->SetTextFont(72);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.3018563,0.32375,"Internal");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetLineWidth(2);
   tex->Draw();
   canvas-7->Modified();
   canvas-7->cd();
   canvas-7->SetSelected(canvas-7);
}
