void HLT_Chi2prob_mean()
{
//=========Macro generated from canvas: canvas-47/histogram
//=========  (Thu Mar 21 17:15:38 2019) by ROOT version6.08/02
   TCanvas *canvas-47 = new TCanvas("canvas-47", "histogram",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   canvas-47->SetHighLightColor(2);
   canvas-47->Range(-172.7222,-0.196875,1061.722,1.115625);
   canvas-47->SetFillColor(0);
   canvas-47->SetBorderMode(0);
   canvas-47->SetBorderSize(2);
   canvas-47->SetLogx();
   canvas-47->SetTickx(1);
   canvas-47->SetTicky(1);
   canvas-47->SetLeftMargin(0.14);
   canvas-47->SetRightMargin(0.05);
   canvas-47->SetTopMargin(0.05);
   canvas-47->SetBottomMargin(0.15);
   canvas-47->SetFrameBorderMode(0);
   canvas-47->SetFrameBorderMode(0);
   Double_t xAxis96[34] = {0.1, 0.1321941, 0.1747528, 0.231013, 0.3053856, 0.4037017, 0.5336699, 0.7054802, 0.9326033, 1.232847, 1.629751, 2.154435, 2.848036, 3.764936, 4.977024, 6.579332, 8.69749, 11.49757, 15.19911, 20.09233, 26.56088, 35.11192, 46.41589, 61.35907, 81.11308, 107.2267, 141.7474, 187.3817, 247.7076, 327.4549, 432.8761, 572.2368, 756.4633, 1000}; 
   
   TH1D *mean__236 = new TH1D("mean__236","",33, xAxis96);
   mean__236->SetEntries(33);
   mean__236->SetStats(0);
   mean__236->SetLineWidth(2);
   mean__236->SetMarkerStyle(20);
   mean__236->SetMarkerSize(1.2);
   mean__236->GetXaxis()->SetTitle("Offline p_{T} [GeV]");
   mean__236->GetXaxis()->SetRange(1,33);
   mean__236->GetXaxis()->SetMoreLogLabels();
   mean__236->GetXaxis()->SetLabelFont(42);
   mean__236->GetXaxis()->SetLabelSize(0.05);
   mean__236->GetXaxis()->SetTitleSize(0.05);
   mean__236->GetXaxis()->SetTitleOffset(1.5);
   mean__236->GetXaxis()->SetTitleFont(42);
   mean__236->GetYaxis()->SetTitle("mean track #Chi^{2} Probability");
   mean__236->GetYaxis()->SetMoreLogLabels();
   mean__236->GetYaxis()->SetLabelFont(42);
   mean__236->GetYaxis()->SetLabelSize(0.05);
   mean__236->GetYaxis()->SetTitleSize(0.05);
   mean__236->GetYaxis()->SetTitleOffset(1.5);
   mean__236->GetYaxis()->SetTitleFont(42);
   mean__236->GetZaxis()->SetLabelFont(42);
   mean__236->GetZaxis()->SetLabelSize(0.05);
   mean__236->GetZaxis()->SetTitleSize(0.05);
   mean__236->GetZaxis()->SetTitleFont(42);
   mean__236->Draw("ep");
   Double_t xAxis97[34] = {0.1, 0.1321941, 0.1747528, 0.231013, 0.3053856, 0.4037017, 0.5336699, 0.7054802, 0.9326033, 1.232847, 1.629751, 2.154435, 2.848036, 3.764936, 4.977024, 6.579332, 8.69749, 11.49757, 15.19911, 20.09233, 26.56088, 35.11192, 46.41589, 61.35907, 81.11308, 107.2267, 141.7474, 187.3817, 247.7076, 327.4549, 432.8761, 572.2368, 756.4633, 1000}; 
   
   TH1D *mean__237 = new TH1D("mean__237","",33, xAxis97);
   mean__237->SetEntries(33);
   mean__237->SetDirectory(0);
   mean__237->SetStats(0);
   mean__237->SetLineStyle(2);
   mean__237->SetLineWidth(2);
   mean__237->SetMarkerStyle(0);
   mean__237->SetMarkerSize(1.2);
   mean__237->GetXaxis()->SetTitle("Offline p_{T} [GeV]");
   mean__237->GetXaxis()->SetRange(1,33);
   mean__237->GetXaxis()->SetLabelFont(42);
   mean__237->GetXaxis()->SetLabelSize(0.05);
   mean__237->GetXaxis()->SetTitleSize(0.05);
   mean__237->GetXaxis()->SetTitleOffset(1.5);
   mean__237->GetXaxis()->SetTitleFont(42);
   mean__237->GetYaxis()->SetTitle("mean track #Chi^{2} Probability");
   mean__237->GetYaxis()->SetMoreLogLabels();
   mean__237->GetYaxis()->SetLabelFont(42);
   mean__237->GetYaxis()->SetLabelSize(0.05);
   mean__237->GetYaxis()->SetTitleSize(0.05);
   mean__237->GetYaxis()->SetTitleOffset(1.5);
   mean__237->GetYaxis()->SetTitleFont(42);
   mean__237->GetZaxis()->SetLabelFont(42);
   mean__237->GetZaxis()->SetLabelSize(0.05);
   mean__237->GetZaxis()->SetTitleSize(0.05);
   mean__237->GetZaxis()->SetTitleFont(42);
   mean__237->Draw("hist same][");
   Double_t xAxis98[34] = {0.1, 0.1321941, 0.1747528, 0.231013, 0.3053856, 0.4037017, 0.5336699, 0.7054802, 0.9326033, 1.232847, 1.629751, 2.154435, 2.848036, 3.764936, 4.977024, 6.579332, 8.69749, 11.49757, 15.19911, 20.09233, 26.56088, 35.11192, 46.41589, 61.35907, 81.11308, 107.2267, 141.7474, 187.3817, 247.7076, 327.4549, 432.8761, 572.2368, 756.4633, 1000}; 
   
   TH1D *mean__238 = new TH1D("mean__238","",33, xAxis98);
   mean__238->SetEntries(33);
   mean__238->SetStats(0);
   mean__238->SetLineWidth(2);
   mean__238->SetMarkerStyle(20);
   mean__238->SetMarkerSize(1.2);
   mean__238->GetXaxis()->SetTitle("Offline p_{T} [GeV]");
   mean__238->GetXaxis()->SetRange(1,33);
   mean__238->GetXaxis()->SetMoreLogLabels();
   mean__238->GetXaxis()->SetLabelFont(42);
   mean__238->GetXaxis()->SetLabelSize(0.05);
   mean__238->GetXaxis()->SetTitleSize(0.05);
   mean__238->GetXaxis()->SetTitleOffset(1.5);
   mean__238->GetXaxis()->SetTitleFont(42);
   mean__238->GetYaxis()->SetTitle("mean track #Chi^{2} Probability");
   mean__238->GetYaxis()->SetMoreLogLabels();
   mean__238->GetYaxis()->SetLabelFont(42);
   mean__238->GetYaxis()->SetLabelSize(0.05);
   mean__238->GetYaxis()->SetTitleSize(0.05);
   mean__238->GetYaxis()->SetTitleOffset(1.5);
   mean__238->GetYaxis()->SetTitleFont(42);
   mean__238->GetZaxis()->SetLabelFont(42);
   mean__238->GetZaxis()->SetLabelSize(0.05);
   mean__238->GetZaxis()->SetTitleSize(0.05);
   mean__238->GetZaxis()->SetTitleFont(42);
   mean__238->Draw("ep same");
   
   TLegend *leg = new TLegend(0.18,0.62,0.28,0.735,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.04);
   leg->SetLineColor(0);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(3000);
   TLegendEntry *entry=leg->AddEntry("mean","Fast track finder","p");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1.2);
   entry->SetTextFont(42);
   entry=leg->AddEntry("mean","Precision tracking","p");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   leg->Draw();
   Double_t xAxis99[34] = {0.1, 0.1321941, 0.1747528, 0.231013, 0.3053856, 0.4037017, 0.5336699, 0.7054802, 0.9326033, 1.232847, 1.629751, 2.154435, 2.848036, 3.764936, 4.977024, 6.579332, 8.69749, 11.49757, 15.19911, 20.09233, 26.56088, 35.11192, 46.41589, 61.35907, 81.11308, 107.2267, 141.7474, 187.3817, 247.7076, 327.4549, 432.8761, 572.2368, 756.4633, 1000}; 
   
   TH1D *mean__239 = new TH1D("mean__239","Mean",33, xAxis99);
   mean__239->SetEntries(33);
   mean__239->SetDirectory(0);
   mean__239->SetStats(0);
   mean__239->SetLineColor(2);
   mean__239->SetLineStyle(2);
   mean__239->SetLineWidth(2);
   mean__239->SetMarkerStyle(0);
   mean__239->SetMarkerSize(1.2);
   mean__239->GetXaxis()->SetTitle("Offline p_{T} [GeV]");
   mean__239->GetXaxis()->SetRange(1,33);
   mean__239->GetXaxis()->SetLabelFont(42);
   mean__239->GetXaxis()->SetLabelSize(0.05);
   mean__239->GetXaxis()->SetTitleSize(0.05);
   mean__239->GetXaxis()->SetTitleOffset(1.5);
   mean__239->GetXaxis()->SetTitleFont(42);
   mean__239->GetYaxis()->SetTitle("mean track #Chi^{2} Probability");
   mean__239->GetYaxis()->SetMoreLogLabels();
   mean__239->GetYaxis()->SetLabelFont(42);
   mean__239->GetYaxis()->SetLabelSize(0.05);
   mean__239->GetYaxis()->SetTitleSize(0.05);
   mean__239->GetYaxis()->SetTitleOffset(1.5);
   mean__239->GetYaxis()->SetTitleFont(42);
   mean__239->GetZaxis()->SetLabelFont(42);
   mean__239->GetZaxis()->SetLabelSize(0.05);
   mean__239->GetZaxis()->SetTitleSize(0.05);
   mean__239->GetZaxis()->SetTitleFont(42);
   mean__239->Draw("hist same][");
   Double_t xAxis100[34] = {0.1, 0.1321941, 0.1747528, 0.231013, 0.3053856, 0.4037017, 0.5336699, 0.7054802, 0.9326033, 1.232847, 1.629751, 2.154435, 2.848036, 3.764936, 4.977024, 6.579332, 8.69749, 11.49757, 15.19911, 20.09233, 26.56088, 35.11192, 46.41589, 61.35907, 81.11308, 107.2267, 141.7474, 187.3817, 247.7076, 327.4549, 432.8761, 572.2368, 756.4633, 1000}; 
   
   TH1D *mean__240 = new TH1D("mean__240","Mean",33, xAxis100);
   mean__240->SetEntries(33);
   mean__240->SetStats(0);
   mean__240->SetLineColor(2);
   mean__240->SetLineWidth(2);
   mean__240->SetMarkerColor(2);
   mean__240->SetMarkerStyle(24);
   mean__240->SetMarkerSize(1.2);
   mean__240->GetXaxis()->SetTitle("Offline p_{T} [GeV]");
   mean__240->GetXaxis()->SetRange(1,33);
   mean__240->GetXaxis()->SetLabelFont(42);
   mean__240->GetXaxis()->SetLabelSize(0.05);
   mean__240->GetXaxis()->SetTitleSize(0.05);
   mean__240->GetXaxis()->SetTitleOffset(1.5);
   mean__240->GetXaxis()->SetTitleFont(42);
   mean__240->GetYaxis()->SetTitle("mean track #Chi^{2} Probability");
   mean__240->GetYaxis()->SetMoreLogLabels();
   mean__240->GetYaxis()->SetLabelFont(42);
   mean__240->GetYaxis()->SetLabelSize(0.05);
   mean__240->GetYaxis()->SetTitleSize(0.05);
   mean__240->GetYaxis()->SetTitleOffset(1.5);
   mean__240->GetYaxis()->SetTitleFont(42);
   mean__240->GetZaxis()->SetLabelFont(42);
   mean__240->GetZaxis()->SetLabelSize(0.05);
   mean__240->GetZaxis()->SetTitleSize(0.05);
   mean__240->GetZaxis()->SetTitleFont(42);
   mean__240->Draw("ep same");
   
   leg = new TLegend(0.18,0.62,0.28,0.735,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.04);
   leg->SetLineColor(0);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(3000);
   entry=leg->AddEntry("mean","Fast track finder","p");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1.2);
   entry->SetTextFont(42);
   entry=leg->AddEntry("mean","Precision tracking","p");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   leg->Draw();
   TLatex *   tex = new TLatex(0.18,0.87875,"ATLAS");
tex->SetNDC();
   tex->SetTextFont(72);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.3018563,0.87875,"Internal");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.18,0.82125,"#bf{Points:} parametrised doublet-seed filter");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.04);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.18,0.76375,"#bf{Dashed histogram:} no parametrisation");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.04);
   tex->SetLineWidth(2);
   tex->Draw();
   canvas-47->Modified();
   canvas-47->cd();
   canvas-47->SetSelected(canvas-47);
}
