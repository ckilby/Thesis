void HLT_rzed_vs_ipt_sigma()
{
//=========Macro generated from canvas: canvas-36/histogram
//=========  (Thu Mar 21 17:11:18 2019) by ROOT version6.08/02
   TCanvas *canvas-36 = new TCanvas("canvas-36", "histogram",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   canvas-36->SetHighLightColor(2);
   canvas-36->Range(-0.6728395,-0.0891692,0.5617284,0.5263716);
   canvas-36->SetFillColor(0);
   canvas-36->SetBorderMode(0);
   canvas-36->SetBorderSize(2);
   canvas-36->SetTickx(1);
   canvas-36->SetTicky(1);
   canvas-36->SetLeftMargin(0.14);
   canvas-36->SetRightMargin(0.05);
   canvas-36->SetTopMargin(0.05);
   canvas-36->SetBottomMargin(0.15);
   canvas-36->SetFrameBorderMode(0);
   canvas-36->SetFrameBorderMode(0);
   Double_t xAxis71[21] = {-0.5, -0.45, -0.4, -0.35, -0.3, -0.25, -0.2, -0.15, -0.1, -0.05, 0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5}; 
   
   TH1D *sigma__181 = new TH1D("sigma__181","",20, xAxis71);
   sigma__181->SetBinContent(1,0.2514635);
   sigma__181->SetBinContent(2,0.1063088);
   sigma__181->SetBinContent(3,0.1615472);
   sigma__181->SetBinContent(4,0.239539);
   sigma__181->SetBinContent(5,0.1354145);
   sigma__181->SetBinContent(6,0.1306806);
   sigma__181->SetBinContent(7,0.09208944);
   sigma__181->SetBinContent(8,0.07137683);
   sigma__181->SetBinContent(9,0.06697985);
   sigma__181->SetBinContent(10,0.06772839);
   sigma__181->SetBinContent(11,0.06726455);
   sigma__181->SetBinContent(12,0.06347869);
   sigma__181->SetBinContent(13,0.1029446);
   sigma__181->SetBinContent(14,0.06854271);
   sigma__181->SetBinContent(15,0.123164);
   sigma__181->SetBinContent(16,0.09065691);
   sigma__181->SetBinContent(17,0.1592262);
   sigma__181->SetBinContent(18,0.1047435);
   sigma__181->SetBinContent(20,0.1230175);
   sigma__181->SetBinError(1,0.05016219);
   sigma__181->SetBinError(2,0.02128864);
   sigma__181->SetBinError(3,0.03021374);
   sigma__181->SetBinError(4,0.03618937);
   sigma__181->SetBinError(5,0.02089954);
   sigma__181->SetBinError(6,0.01622542);
   sigma__181->SetBinError(7,0.00952843);
   sigma__181->SetBinError(8,0.005200175);
   sigma__181->SetBinError(9,0.003360848);
   sigma__181->SetBinError(10,0.003517467);
   sigma__181->SetBinError(11,0.003245188);
   sigma__181->SetBinError(12,0.003010405);
   sigma__181->SetBinError(13,0.007915497);
   sigma__181->SetBinError(14,0.00652248);
   sigma__181->SetBinError(15,0.01508322);
   sigma__181->SetBinError(16,0.01286591);
   sigma__181->SetBinError(17,0.02977333);
   sigma__181->SetBinError(18,0.01841857);
   sigma__181->SetBinError(20,0.02817112);
   sigma__181->SetMinimum(0.003161933);
   sigma__181->SetMaximum(0.4955946);
   sigma__181->SetEntries(20);
   sigma__181->SetStats(0);
   sigma__181->SetLineWidth(2);
   sigma__181->SetMarkerStyle(20);
   sigma__181->SetMarkerSize(1.2);
   sigma__181->GetXaxis()->SetTitle("1/p_{T} [GeV^{-1}]");
   sigma__181->GetXaxis()->SetMoreLogLabels();
   sigma__181->GetXaxis()->SetLabelFont(42);
   sigma__181->GetXaxis()->SetLabelSize(0.05);
   sigma__181->GetXaxis()->SetTitleSize(0.05);
   sigma__181->GetXaxis()->SetTitleOffset(1.5);
   sigma__181->GetXaxis()->SetTitleFont(42);
   sigma__181->GetYaxis()->SetTitle("z_{0} resolution [mm]");
   sigma__181->GetYaxis()->SetMoreLogLabels();
   sigma__181->GetYaxis()->SetLabelFont(42);
   sigma__181->GetYaxis()->SetLabelSize(0.05);
   sigma__181->GetYaxis()->SetTitleSize(0.05);
   sigma__181->GetYaxis()->SetTitleOffset(1.5);
   sigma__181->GetYaxis()->SetTitleFont(42);
   sigma__181->GetZaxis()->SetLabelFont(42);
   sigma__181->GetZaxis()->SetLabelSize(0.05);
   sigma__181->GetZaxis()->SetTitleSize(0.05);
   sigma__181->GetZaxis()->SetTitleFont(42);
   sigma__181->Draw("ep");
   Double_t xAxis72[21] = {-0.5, -0.45, -0.4, -0.35, -0.3, -0.25, -0.2, -0.15, -0.1, -0.05, 0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5}; 
   
   TH1D *sigma__182 = new TH1D("sigma__182","",20, xAxis72);
   sigma__182->SetBinContent(1,0.2514635);
   sigma__182->SetBinContent(2,0.1063088);
   sigma__182->SetBinContent(3,0.1615472);
   sigma__182->SetBinContent(4,0.239539);
   sigma__182->SetBinContent(5,0.1354145);
   sigma__182->SetBinContent(6,0.1296899);
   sigma__182->SetBinContent(7,0.09208944);
   sigma__182->SetBinContent(8,0.07689726);
   sigma__182->SetBinContent(9,0.06849813);
   sigma__182->SetBinContent(10,0.06772839);
   sigma__182->SetBinContent(11,0.06726455);
   sigma__182->SetBinContent(12,0.06470958);
   sigma__182->SetBinContent(13,0.1012181);
   sigma__182->SetBinContent(14,0.06854271);
   sigma__182->SetBinContent(15,0.1206818);
   sigma__182->SetBinContent(16,0.09065691);
   sigma__182->SetBinContent(17,0.1592262);
   sigma__182->SetBinContent(18,0.1047435);
   sigma__182->SetBinContent(20,0.1230175);
   sigma__182->SetBinError(1,0.05016219);
   sigma__182->SetBinError(2,0.02128864);
   sigma__182->SetBinError(3,0.03021374);
   sigma__182->SetBinError(4,0.03618937);
   sigma__182->SetBinError(5,0.02089954);
   sigma__182->SetBinError(6,0.01610096);
   sigma__182->SetBinError(7,0.00952843);
   sigma__182->SetBinError(8,0.005574708);
   sigma__182->SetBinError(9,0.003428286);
   sigma__182->SetBinError(10,0.003517467);
   sigma__182->SetBinError(11,0.003245188);
   sigma__182->SetBinError(12,0.003061981);
   sigma__182->SetBinError(13,0.00769677);
   sigma__182->SetBinError(14,0.00652248);
   sigma__182->SetBinError(15,0.01457238);
   sigma__182->SetBinError(16,0.01286591);
   sigma__182->SetBinError(17,0.02977333);
   sigma__182->SetBinError(18,0.01841857);
   sigma__182->SetBinError(20,0.02817112);
   sigma__182->SetMinimum(0.003161933);
   sigma__182->SetMaximum(0.4955946);
   sigma__182->SetEntries(20);
   sigma__182->SetDirectory(0);
   sigma__182->SetStats(0);
   sigma__182->SetLineStyle(2);
   sigma__182->SetLineWidth(2);
   sigma__182->SetMarkerStyle(0);
   sigma__182->SetMarkerSize(1.2);
   sigma__182->GetXaxis()->SetTitle("1/p_{T} [GeV^{-1}]");
   sigma__182->GetXaxis()->SetLabelFont(42);
   sigma__182->GetXaxis()->SetLabelSize(0.05);
   sigma__182->GetXaxis()->SetTitleSize(0.05);
   sigma__182->GetXaxis()->SetTitleOffset(1.5);
   sigma__182->GetXaxis()->SetTitleFont(42);
   sigma__182->GetYaxis()->SetTitle("z_{0} resolution [mm]");
   sigma__182->GetYaxis()->SetMoreLogLabels();
   sigma__182->GetYaxis()->SetLabelFont(42);
   sigma__182->GetYaxis()->SetLabelSize(0.05);
   sigma__182->GetYaxis()->SetTitleSize(0.05);
   sigma__182->GetYaxis()->SetTitleOffset(1.5);
   sigma__182->GetYaxis()->SetTitleFont(42);
   sigma__182->GetZaxis()->SetLabelFont(42);
   sigma__182->GetZaxis()->SetLabelSize(0.05);
   sigma__182->GetZaxis()->SetTitleSize(0.05);
   sigma__182->GetZaxis()->SetTitleFont(42);
   sigma__182->Draw("hist same][");
   Double_t xAxis73[21] = {-0.5, -0.45, -0.4, -0.35, -0.3, -0.25, -0.2, -0.15, -0.1, -0.05, 0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5}; 
   
   TH1D *sigma__183 = new TH1D("sigma__183","",20, xAxis73);
   sigma__183->SetBinContent(1,0.2514635);
   sigma__183->SetBinContent(2,0.1063088);
   sigma__183->SetBinContent(3,0.1615472);
   sigma__183->SetBinContent(4,0.239539);
   sigma__183->SetBinContent(5,0.1354145);
   sigma__183->SetBinContent(6,0.1306806);
   sigma__183->SetBinContent(7,0.09208944);
   sigma__183->SetBinContent(8,0.07137683);
   sigma__183->SetBinContent(9,0.06697985);
   sigma__183->SetBinContent(10,0.06772839);
   sigma__183->SetBinContent(11,0.06726455);
   sigma__183->SetBinContent(12,0.06347869);
   sigma__183->SetBinContent(13,0.1029446);
   sigma__183->SetBinContent(14,0.06854271);
   sigma__183->SetBinContent(15,0.123164);
   sigma__183->SetBinContent(16,0.09065691);
   sigma__183->SetBinContent(17,0.1592262);
   sigma__183->SetBinContent(18,0.1047435);
   sigma__183->SetBinContent(20,0.1230175);
   sigma__183->SetBinError(1,0.05016219);
   sigma__183->SetBinError(2,0.02128864);
   sigma__183->SetBinError(3,0.03021374);
   sigma__183->SetBinError(4,0.03618937);
   sigma__183->SetBinError(5,0.02089954);
   sigma__183->SetBinError(6,0.01622542);
   sigma__183->SetBinError(7,0.00952843);
   sigma__183->SetBinError(8,0.005200175);
   sigma__183->SetBinError(9,0.003360848);
   sigma__183->SetBinError(10,0.003517467);
   sigma__183->SetBinError(11,0.003245188);
   sigma__183->SetBinError(12,0.003010405);
   sigma__183->SetBinError(13,0.007915497);
   sigma__183->SetBinError(14,0.00652248);
   sigma__183->SetBinError(15,0.01508322);
   sigma__183->SetBinError(16,0.01286591);
   sigma__183->SetBinError(17,0.02977333);
   sigma__183->SetBinError(18,0.01841857);
   sigma__183->SetBinError(20,0.02817112);
   sigma__183->SetMinimum(0.003161933);
   sigma__183->SetMaximum(0.4955946);
   sigma__183->SetEntries(20);
   sigma__183->SetStats(0);
   sigma__183->SetLineWidth(2);
   sigma__183->SetMarkerStyle(20);
   sigma__183->SetMarkerSize(1.2);
   sigma__183->GetXaxis()->SetTitle("1/p_{T} [GeV^{-1}]");
   sigma__183->GetXaxis()->SetMoreLogLabels();
   sigma__183->GetXaxis()->SetLabelFont(42);
   sigma__183->GetXaxis()->SetLabelSize(0.05);
   sigma__183->GetXaxis()->SetTitleSize(0.05);
   sigma__183->GetXaxis()->SetTitleOffset(1.5);
   sigma__183->GetXaxis()->SetTitleFont(42);
   sigma__183->GetYaxis()->SetTitle("z_{0} resolution [mm]");
   sigma__183->GetYaxis()->SetMoreLogLabels();
   sigma__183->GetYaxis()->SetLabelFont(42);
   sigma__183->GetYaxis()->SetLabelSize(0.05);
   sigma__183->GetYaxis()->SetTitleSize(0.05);
   sigma__183->GetYaxis()->SetTitleOffset(1.5);
   sigma__183->GetYaxis()->SetTitleFont(42);
   sigma__183->GetZaxis()->SetLabelFont(42);
   sigma__183->GetZaxis()->SetLabelSize(0.05);
   sigma__183->GetZaxis()->SetTitleSize(0.05);
   sigma__183->GetZaxis()->SetTitleFont(42);
   sigma__183->Draw("ep same");
   
   TLegend *leg = new TLegend(0.18,0.62,0.28,0.735,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.04);
   leg->SetLineColor(0);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(3000);
   TLegendEntry *entry=leg->AddEntry("sigma","Fast track finder","p");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1.2);
   entry->SetTextFont(42);
   entry=leg->AddEntry("sigma","Precision tracking","p");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   leg->Draw();
   Double_t xAxis74[21] = {-0.5, -0.45, -0.4, -0.35, -0.3, -0.25, -0.2, -0.15, -0.1, -0.05, 0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5}; 
   
   TH1D *sigma__184 = new TH1D("sigma__184","RMS",20, xAxis74);
   sigma__184->SetBinContent(1,0.03407585);
   sigma__184->SetBinContent(2,0.03458829);
   sigma__184->SetBinContent(3,0.02720325);
   sigma__184->SetBinContent(4,0.2622243);
   sigma__184->SetBinContent(5,0.03669761);
   sigma__184->SetBinContent(6,0.03521926);
   sigma__184->SetBinContent(7,0.0277351);
   sigma__184->SetBinContent(8,0.0342962);
   sigma__184->SetBinContent(9,0.03179505);
   sigma__184->SetBinContent(10,0.02960914);
   sigma__184->SetBinContent(11,0.03066525);
   sigma__184->SetBinContent(12,0.02958271);
   sigma__184->SetBinContent(13,0.0269774);
   sigma__184->SetBinContent(14,0.02952502);
   sigma__184->SetBinContent(15,0.04057677);
   sigma__184->SetBinContent(16,0.02833177);
   sigma__184->SetBinContent(17,0.03482666);
   sigma__184->SetBinContent(18,0.04682784);
   sigma__184->SetBinContent(20,0.02671306);
   sigma__184->SetBinError(1,0.006682823);
   sigma__184->SetBinError(2,0.006783323);
   sigma__184->SetBinError(3,0.004966611);
   sigma__184->SetBinError(4,0.0395587);
   sigma__184->SetBinError(5,0.005662565);
   sigma__184->SetBinError(6,0.004375333);
   sigma__184->SetBinError(7,0.002830701);
   sigma__184->SetBinError(8,0.002499436);
   sigma__184->SetBinError(9,0.00159457);
   sigma__184->SetBinError(10,0.001535163);
   sigma__184->SetBinError(11,0.001475383);
   sigma__184->SetBinError(12,0.001385337);
   sigma__184->SetBinError(13,0.002057007);
   sigma__184->SetBinError(14,0.002765272);
   sigma__184->SetBinError(15,0.004900963);
   sigma__184->SetBinError(16,0.004006717);
   sigma__184->SetBinError(17,0.006481256);
   sigma__184->SetBinError(18,0.008231925);
   sigma__184->SetBinError(20,0.005973222);
   sigma__184->SetMinimum(0.003161933);
   sigma__184->SetMaximum(0.4955946);
   sigma__184->SetEntries(20);
   sigma__184->SetDirectory(0);
   sigma__184->SetStats(0);
   sigma__184->SetLineColor(2);
   sigma__184->SetLineStyle(2);
   sigma__184->SetLineWidth(2);
   sigma__184->SetMarkerStyle(0);
   sigma__184->SetMarkerSize(1.2);
   sigma__184->GetXaxis()->SetTitle("1/p_{T} [GeV^{-1}]");
   sigma__184->GetXaxis()->SetLabelFont(42);
   sigma__184->GetXaxis()->SetLabelSize(0.05);
   sigma__184->GetXaxis()->SetTitleSize(0.05);
   sigma__184->GetXaxis()->SetTitleOffset(1.5);
   sigma__184->GetXaxis()->SetTitleFont(42);
   sigma__184->GetYaxis()->SetTitle("z_{0} resolution [mm]");
   sigma__184->GetYaxis()->SetMoreLogLabels();
   sigma__184->GetYaxis()->SetLabelFont(42);
   sigma__184->GetYaxis()->SetLabelSize(0.05);
   sigma__184->GetYaxis()->SetTitleSize(0.05);
   sigma__184->GetYaxis()->SetTitleOffset(1.5);
   sigma__184->GetYaxis()->SetTitleFont(42);
   sigma__184->GetZaxis()->SetLabelFont(42);
   sigma__184->GetZaxis()->SetLabelSize(0.05);
   sigma__184->GetZaxis()->SetTitleSize(0.05);
   sigma__184->GetZaxis()->SetTitleFont(42);
   sigma__184->Draw("hist same][");
   Double_t xAxis75[21] = {-0.5, -0.45, -0.4, -0.35, -0.3, -0.25, -0.2, -0.15, -0.1, -0.05, 0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5}; 
   
   TH1D *sigma__185 = new TH1D("sigma__185","RMS",20, xAxis75);
   sigma__185->SetBinContent(1,0.03407585);
   sigma__185->SetBinContent(2,0.03458829);
   sigma__185->SetBinContent(3,0.02720325);
   sigma__185->SetBinContent(4,0.2622243);
   sigma__185->SetBinContent(5,0.03669761);
   sigma__185->SetBinContent(6,0.03562374);
   sigma__185->SetBinContent(7,0.0277351);
   sigma__185->SetBinContent(8,0.03366872);
   sigma__185->SetBinContent(9,0.03186708);
   sigma__185->SetBinContent(10,0.02960914);
   sigma__185->SetBinContent(11,0.03066525);
   sigma__185->SetBinContent(12,0.02958271);
   sigma__185->SetBinContent(13,0.02715954);
   sigma__185->SetBinContent(14,0.02952502);
   sigma__185->SetBinContent(15,0.04105625);
   sigma__185->SetBinContent(16,0.02833177);
   sigma__185->SetBinContent(17,0.03482666);
   sigma__185->SetBinContent(18,0.04682784);
   sigma__185->SetBinContent(20,0.02671306);
   sigma__185->SetBinError(1,0.006682823);
   sigma__185->SetBinError(2,0.006783323);
   sigma__185->SetBinError(3,0.004966611);
   sigma__185->SetBinError(4,0.0395587);
   sigma__185->SetBinError(5,0.005662565);
   sigma__185->SetBinError(6,0.004491971);
   sigma__185->SetBinError(7,0.002830701);
   sigma__185->SetBinError(8,0.002466132);
   sigma__185->SetBinError(9,0.001602008);
   sigma__185->SetBinError(10,0.001535163);
   sigma__185->SetBinError(11,0.001475383);
   sigma__185->SetBinError(12,0.001385337);
   sigma__185->SetBinError(13,0.002095404);
   sigma__185->SetBinError(14,0.002765272);
   sigma__185->SetBinError(15,0.005027873);
   sigma__185->SetBinError(16,0.004006717);
   sigma__185->SetBinError(17,0.006481256);
   sigma__185->SetBinError(18,0.008231925);
   sigma__185->SetBinError(20,0.005973222);
   sigma__185->SetMinimum(0.003161933);
   sigma__185->SetMaximum(0.4955946);
   sigma__185->SetEntries(20);
   sigma__185->SetStats(0);
   sigma__185->SetLineColor(2);
   sigma__185->SetLineWidth(2);
   sigma__185->SetMarkerColor(2);
   sigma__185->SetMarkerStyle(24);
   sigma__185->SetMarkerSize(1.2);
   sigma__185->GetXaxis()->SetTitle("1/p_{T} [GeV^{-1}]");
   sigma__185->GetXaxis()->SetLabelFont(42);
   sigma__185->GetXaxis()->SetLabelSize(0.05);
   sigma__185->GetXaxis()->SetTitleSize(0.05);
   sigma__185->GetXaxis()->SetTitleOffset(1.5);
   sigma__185->GetXaxis()->SetTitleFont(42);
   sigma__185->GetYaxis()->SetTitle("z_{0} resolution [mm]");
   sigma__185->GetYaxis()->SetMoreLogLabels();
   sigma__185->GetYaxis()->SetLabelFont(42);
   sigma__185->GetYaxis()->SetLabelSize(0.05);
   sigma__185->GetYaxis()->SetTitleSize(0.05);
   sigma__185->GetYaxis()->SetTitleOffset(1.5);
   sigma__185->GetYaxis()->SetTitleFont(42);
   sigma__185->GetZaxis()->SetLabelFont(42);
   sigma__185->GetZaxis()->SetLabelSize(0.05);
   sigma__185->GetZaxis()->SetTitleSize(0.05);
   sigma__185->GetZaxis()->SetTitleFont(42);
   sigma__185->Draw("ep same");
   
   leg = new TLegend(0.18,0.62,0.28,0.735,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.04);
   leg->SetLineColor(0);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(3000);
   entry=leg->AddEntry("sigma","Fast track finder","p");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1.2);
   entry->SetTextFont(42);
   entry=leg->AddEntry("sigma","Precision tracking","p");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   leg->Draw();
   TLatex *   tex = new TLatex(0.18,0.87875,"ATLAS");
tex->SetNDC();
   tex->SetTextFont(72);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.3018563,0.87875,"Internal");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.18,0.82125,"#bf{Points:} parametrised second-stage RoI");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.04);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.18,0.76375,"#bf{Dashed histogram:} no parametrisation");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.04);
   tex->SetLineWidth(2);
   tex->Draw();
   canvas-36->Modified();
   canvas-36->cd();
   canvas-36->SetSelected(canvas-36);
}
